import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  messages = [
    {
      user: 'simon',
      createdAt:1554090856000,
      msg: 'test message 1'
    },
    {
      user: 'max',
      createdAt:1554090856000,
      msg: 'test message 2'
    },
    {
      user: 'simon',
      createdAt:1554090856000,
      msg: 'test message 3'
    }
  ];

  currentUser = 'simon';

  newMsg = '';

  @ViewChild(IonContent) content : IonContent;

  constructor(){}

  sendMessage(){
    this.messages.push({
      user: this.currentUser,
      createdAt: new Date().getTime(),
      msg: this.newMsg
    });

    this.newMsg = '';

    setTimeout(() => {
      this.content.scrollToBottom(200);
    });
    
  }
}
